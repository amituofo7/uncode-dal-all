package cn.uncode.dal.core;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import cn.uncode.dal.criteria.QueryCriteria;
import cn.uncode.dal.descriptor.QueryResult;

public interface BaseDAL {
	
	int NO_CACHE = -2;
	
	int PERSISTENT_CACHE = 0;
	
	String PAGE_INDEX_KEY = "pageIndex";
	String PAGE_SIZE_KEY = "pageSize";
	String PAGE_COUNT_KEY = "pageCount";
	String RECORD_TOTAL_KEY = "recordTotal";
    
	//-------------------------
	// selectByCriteria
	//-------------------------
	/**
	 * 根据条件查询结果集
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * List<String> fields = new ArrayList<>();
	 * fields.add("id");
	 * fields.add("name");
	 * QueryResult queryResult  = selectByCriteria(fields, queryCriteria);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 查询结果对象
	 */
    QueryResult selectByCriteria(List<String> fields, QueryCriteria queryCriteria);
    
	/**
	 * 根据条件查询结果集
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * QueryResult queryResult  = selectByCriteria(new String[]{id, name}, queryCriteria);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 查询结果对象
	 */
    QueryResult selectByCriteria(String[] fields, QueryCriteria queryCriteria);
    
	/**
	 * 根据条件查询结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * List<String> fields = new ArrayList<>();
	 * fields.add("id");
	 * fields.add("name");
	 * QueryResult queryResult  = selectByCriteria(fields, queryCriteria, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果对象
	 */
    QueryResult selectByCriteria(List<String> fields, QueryCriteria queryCriteria, int seconds);
    
	/**
	 * 根据条件查询结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * QueryResult queryResult  = selectByCriteria(new String[]{id, name}, queryCriteria, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果对象
	 */
    QueryResult selectByCriteria(String[] fields, QueryCriteria queryCriteria, int seconds);
    
	/**
	 * 根据条件查询结果集
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * QueryResult queryResult  = selectByCriteria(queryCriteria);
	 * </pre>
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 查询结果对象
	 */
    QueryResult selectByCriteria(QueryCriteria queryCriteria);
    
	/**
	 * 根据条件查询结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * QueryResult queryResult  = selectByCriteria(queryCriteria, 10);
	 * </pre>
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果对象
	 */
    QueryResult selectByCriteria(QueryCriteria queryCriteria, int seconds);
    
	/**
	 * 根据条件查询分页结果集
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * List<String> fields = new ArrayList<>();
	 * fields.add("id");
	 * fields.add("name");
	 * queryCriteria.setPageIndex(1);
	 * queryCriteria.setPageSize(30);
	 * QueryResult queryResult  = selectByCriteria(fields, queryCriteria);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 查询结果对象
	 */
    QueryResult selectPageByCriteria(List<String> fields, QueryCriteria queryCriteria);
    
	/**
	 * 根据条件查询分页结果集
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * queryCriteria.setPageIndex(1);
	 * queryCriteria.setPageSize(30);
	 * QueryResult queryResult  = selectByCriteria(new String[]{id, name}, queryCriteria);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 查询结果对象带分页数据
	 */
    QueryResult selectPageByCriteria(String[] fields, QueryCriteria queryCriteria);
    
	/**
	 * 根据条件查询分页结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * List<String> fields = new ArrayList<>();
	 * fields.add("id");
	 * fields.add("name");
	 * queryCriteria.setPageIndex(1);
	 * queryCriteria.setPageSize(30);
	 * QueryResult queryResult  = selectByCriteria(fields, queryCriteria， 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果对象带分页数据
	 */
    QueryResult selectPageByCriteria(List<String> fields, QueryCriteria queryCriteria, int seconds);
    
	/**
	 * 根据条件查询分页结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * queryCriteria.setPageIndex(1);
	 * queryCriteria.setPageSize(30);
	 * QueryResult queryResult  = selectByCriteria(new String[]{id, name}, queryCriteria, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果对象带分页数据
	 */
    QueryResult selectPageByCriteria(String[] fields, QueryCriteria queryCriteria, int seconds);
    
	/**
	 * 根据条件查询分页结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * queryCriteria.setPageIndex(1);
	 * queryCriteria.setPageSize(30);
	 * QueryResult queryResult  = selectByCriteria(queryCriteria);
	 * </pre>
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 查询结果对象带分页数据
	 */
    QueryResult selectPageByCriteria(QueryCriteria queryCriteria);
    
	/**
	 * 根据条件查询分页结果集并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * queryCriteria.setPageIndex(1);
	 * queryCriteria.setPageSize(30);
	 * QueryResult queryResult  = selectByCriteria(queryCriteria, 10);
	 * </pre>
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果对象带分页数据
	 */
    QueryResult selectPageByCriteria(QueryCriteria queryCriteria, int seconds);
    
    //-------------------------
  	// countByCriteria
  	//-------------------------
	/**
	 * 根据条件count数量
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * int count = countByCriteria(queryCriteria);
	 * </pre>
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 记录数
	 */
    int countByCriteria(QueryCriteria queryCriteria);
    
	/**
	 * 根据条件count数量并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * int count = countByCriteria(queryCriteria, 10);
	 * </pre>
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 记录数
	 */
    int countByCriteria(QueryCriteria queryCriteria, int seconds);
    
	/**
	 * 根据条件count数量
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * List<String> fields = new ArrayList<>();
	 * fields.add("id");
	 * fields.add("name");
	 * int count = countByCriteria(fields, queryCriteria);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 记录数
	 */
    int countByCriteria(List<String> fields, QueryCriteria queryCriteria);
    
	/**
	 * 根据条件count数量并设置缓存过期时间
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * List<String> fields = new ArrayList<>();
	 * fields.add("id");
	 * fields.add("name");
	 * int count = countByCriteria(fields, queryCriteria, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param queryCriteria 查询条件{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @param seconds 缓存时间(秒)
	 * @return 记录数
	 */
    int countByCriteria(List<String> fields, QueryCriteria queryCriteria, int seconds);
    
    //-------------------------
  	// selectByPrimaryKey
  	//-------------------------
    /**
	 * 根据主键查询单条记录并设置缓存过期时间
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * QueryResult queryResult = selectByPrimaryKey(user);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param obj 查询对象
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(Object obj);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * QueryResult queryResult = selectByPrimaryKey(fields, user, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param obj 查询对象
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(List<String> fields, Object obj);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * selectByPrimaryKey(new String[]{"id", "name"}, user);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param obj 查询对象
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(String[] fields, Object obj);
    
	/**
	 * 根据主键查询单条记录并设置缓存过期时间
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * selectByPrimaryKey(fields, user, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param obj 查询对象
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(List<String> fields, Object obj, int seconds);
    
	/**
	 * 根据主键查询单条记录并设置缓存过期时间
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * selectByPrimaryKey(new String[]{"id", "name"}, user, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param obj 查询对象
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(String[] fields, Object obj, int seconds);
    
	/**
	 * 根据主键查询单条记录并设置缓存过期时间
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * selectByPrimaryKey(new String[]{"id", "name"}, user, 10);
	 * </pre>
	 * @param fields 需要显示的表字段集合
	 * @param database 数据库
	 * @param obj 查询对象
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(String[] fields, String database, Object obj, int seconds);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * selectByPrimaryKey(User.class, 1);
	 * </pre>
	 * @param clazz 查询对象类
	 * @param id 主键标识
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(Class<?> clazz, Object id);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * selectByPrimaryKey("user", 1);
	 * </pre>
	 * @param table 表名
	 * @param id 主键标识
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(String table, Object id);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * selectByPrimaryKey(User.class, 1);
	 * </pre>
	 * @param clazz 查询对象类
	 * @param id 主键标识
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(Class<?> clazz, Object id, int seconds);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * selectByPrimaryKey("user", 1, 10);
	 * </pre>
	 * @param table 表名
	 * @param id 主键标识
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(String table, Object id, int seconds);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * selectByPrimaryKey(fields, User.class, 1);
	 * </pre>
	 * @param fields 需要显示的字段集合
	 * @param clazz 查询对象类
	 * @param id 主键标识
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(List<String> fields, Class<?> clazz, Object id);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * selectByPrimaryKey(fields, "user", 1);
	 * </pre>
	 * @param fields 需要显示的字段集合
	 * @param table 表名
	 * @param id 主键标识
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(List<String> fields, String table, Object id);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * selectByPrimaryKey(fields, User.class, 1);
	 * </pre>
	 * @param fields 需要显示的字段集合
	 * @param clazz 查询对象类
	 * @param id 主键标识
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(List<String> fields, Class<?> clazz, Object id, int seconds);
    
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * selectByPrimaryKey(fields, User.class, 1);
	 * </pre>
	 * @param fields 需要显示的字段集合
	 * @param table 表名
	 * @param id 主键标识
	 * @param seconds 缓存时间(秒)
	 * @return 查询结果
	 */
    QueryResult selectByPrimaryKey(List<String> fields, String table, Object id, int seconds);
    
    
    //-------------------------
  	// selectByIds
  	//-------------------------
    
    QueryResult selectByIds(List<String> fields, String table, Object... ids);
    
    QueryResult selectByIds(List<String> fields, String table, List<Object> ids);
    
    /**
     * 根据id查询
     * @param fields
     * @param table
     * @param ids
     * @return
     */
    QueryResult selectByIds(List<String> fields, String database, String table, List<Object> ids);
    
    Object save(Object obj);
    
    //-------------------------
  	// insert
  	//-------------------------
	/**
	 * 插入单条记录
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * user.setName("Juny");
	 * insert(user);
	 * </pre>
	 * @param obj 插入对象
	 * @return 生成的主键Id
	 */
    Object insert(Object obj);
    
	/**
	 * 插入单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * user.put("name", "Juny");
	 * insert("user", user);
	 * </pre>
	 * @param table 表名
	 * @param obj 插入对象map
	 * @return 生成的主键Id
	 */
    Object insert(String table, Map<String, Object> obj);
    
	/**
	 * 插入单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * user.put("name", "Juny");
	 * insert("user", user);
	 * </pre>
	 * @param database 数据库名
	 * @param table 表名
	 * @param obj 插入对象map
	 * @return 生成的主键Id
	 */
    Object insert(String database, String table, Map<String, Object> obj);
    
    /**
	 * 批量插入多条记录
	 * <pre>
	 * List<User> users = new ArrayList<User>();
	 * .......
	 * insert(users);
	 * </pre>
	 * @param objs 插入对象集
	 * @return 成功的条数
	 */
    int insertList(List<?> objs);
    
    /**
     * 批量插入多条记录
     * @param table 表名
     * @param objs objs 插入对象集
     * @return 成功的条数
     */
    int insertList(String table, List<Map<String, Object>> objs);
    
	/**
	 * 异步插入单条记录
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * user.setName("Juny");
	 * asynInsert(user);
	 * </pre>
	 * @param obj 插入对象
	 * @return 生成的主键Id
	 */
    void asynInsert(Object obj);
    
	/**
	 * 异步插入单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * user.put("name", "Juny");
	 * insert("user", user);
	 * </pre>
	 * @param table 表名
	 * @param obj 插入对象map
	 * @return 生成的主键Id
	 */
    void asynInsert(String table, Map<String, Object> obj);
    
	/**
	 * 异步插入单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * user.put("name", "Juny");
	 * insert("user", user);
	 * </pre>
	 * @param database 数据库名
	 * @param table 表名
	 * @param obj 插入对象map
	 * @return 生成的主键Id
	 */
    void asynInsert(String database, String table, Map<String, Object> obj);
    
    //-------------------------
  	// update
  	//-------------------------
	/**
	 * 根据条件修改相应记录[多条]
	 * <pre>
	 * User user = new User();
	 * user.setName("HelloWord");
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * int number = updateByCriteria(user, queryCriteria);
	 * </pre>
	 * @param obj 要修改的属性封装，只要需要修改的字段
	 * @param queryCriteria 要修改的条件封装{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 修改成功的条数
	 */
    int updateByCriteria(Object obj, QueryCriteria queryCriteria);
    
	/**
	 * 根据主键修改单条记录
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * user.setName("Juny");
	 * int number = updateByPrimaryKey(user);
	 * </pre>
	 * @param obj 要修改的属性封装及id,只要需要修改的字段
	 * @return 成功为1,失败为0.为0时请注意可能因为该表作了version控制
	 */
    int updateByPrimaryKey(Object obj);
    
	/**
	 * 根据主键修改单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * user.put("name", "Juny");
	 * int number = updateByPrimaryKey("user", user);
	 * </pre>
	 * @param table 表名
	 * @param obj 要修改的属性封装及id,只要需要修改的字段
	 * @return 成功为1,失败为0.为0时请注意可能因为该表作了version控制
	 */
    int updateByPrimaryKey(String table, Map<String, Object> obj);
    
	/**
	 * 根据主键修改单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * user.put("name", "Juny");
	 * int number = updateByPrimaryKey("user", user);
	 * </pre>
	 * @param database 数据库名
	 * @param table 表名
	 * @param obj 要修改的属性封装及id,只要需要修改的字段
	 * @return 成功为1,失败为0.为0时请注意可能因为该表作了version控制
	 */
    int updateByPrimaryKey(String database, String table, Map<String, Object> obj);
    
    
    void asynUpdateByPrimaryKey(String table, Map<String, Object> obj);
    
    //-------------------------
  	// delete
  	//-------------------------
	/**
	 * 根据主键删除单条记录
	 * <pre>
	 * User user = new User();
	 * user.setId(1);
	 * int number = deleteByPrimaryKey(user);
	 * </pre>
	 * @param obj 要删除的对象封装
	 * @return 成功为1,失败为0.
	 */
    int deleteByPrimaryKey(Object obj);
    
	/**
	 * 根据主键删除单条记录
	 * <pre>
	 * Map<String, Object> user = new HashMap<String, Object>();
	 * user.put("id", 1);
	 * int number = deleteByPrimaryKey("user", user);
	 * </pre>
	 * @param table 表名
	 * @param obj 要删除的对象封装
	 * @return 成功为1,失败为0.
	 */
    int deleteByPrimaryKey(String table, Map<String, Object> obj);
    
	/**
	 * 根据主键删除单条记录
	 * @param clazz 要删除的对象实例类
	 * @param obj 主键id
	 * @return 成功为1,失败为0.
	 */
    int deleteByPrimaryKey(Class<?> clazz, Object id);
    
	/**
	 * 根据主键删除单条记录
	 * @param table 表名
	 * @param obj 主键id
	 * @return 成功为1,失败为0.
	 */
    int deleteByPrimaryKey(String table, Object id);
    
	/**
	 * 根据主键删除单条记录
	 * @param database 数据库名
	 * @param table 表名
	 * @param obj 主键id
	 * @return 成功为1,失败为0.
	 */
    int deleteByPrimaryKey(String database, String table, Object id);
    
    /**
	 * 根据条件删除相应记录[多条]
	 * <pre>
	 * QueryCriteria queryCriteria = new QueryCriteria();
	 * queryCriteria.setTable(User.class);
	 * Criteria criteria = queryCriteria.createCriteria();
	 * criteria.andColumnEqualTo("name", "Juny");
	 * int number = updateByCriteria(queryCriteria);
	 * </pre>
	 * @param queryCriteria 要删除的条件封装{@link cn.uncode.dal.criteria.QueryCriteria}
	 * @return 删除成功的条数
	 */
    int deleteByCriteria(QueryCriteria queryCriteria);
    
    //-------------------------
  	// other
  	//-------------------------
    void reloadTable(String tableName);
    
    void clearCache(String tableName);
    
    void reloadTable(String database, String tableName);
    
    void clearCache(String database, String tableName);
    
    void clear(String partten);
    
    void asynClear(String partten);
    
    public JdbcTemplate getTemplate();
    
    
}

package cn.uncode.dal.core;

import java.io.Serializable;
import java.util.Date;


public abstract class BaseDTO implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -1031601349446401649L;
	
	public static final String DTO_SUFFIX = "DTO";
	
	/**
	 * 常量名称不要修改
	 */
	public static final String ID = "pid";
	
	public static final String CREATE_BY="createBy";
	
	public static final String CREATE_AT="createAt";
	
	public static final String MODIFY_BY="modifyBy";
	
	public static final String MODIFY_AT="modifyAt";
	
//	public static final String VERSION = "version";
	
	/** 主键id*/
    protected Long pid;
    /** 版本*/
//    protected Integer version;
    /** 创建人 */
    protected String createBy;
    /** 创建时间 */
    protected Date createAt;
    /** 修改人 */
    protected String modifyBy;
    /** 修改时间 */
    protected Date modifyAt;
    
    public BaseDTO() {
    	setCreateAt(new Date());
    	setModifyAt(new Date());
    }

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyAt() {
		return modifyAt;
	}

	public void setModifyAt(Date modifyAt) {
		this.modifyAt = modifyAt;
	}
    
	

	
	
}

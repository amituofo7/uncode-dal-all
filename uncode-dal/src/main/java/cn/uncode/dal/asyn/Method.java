package cn.uncode.dal.asyn;

public enum Method {
	
	SELECT_BY_CRITERIA(1),
	
	COUNT_BY_CRITERIA(5),
	
	SELECT_BY_PRIMARY_KEY(6),SELECT_BY_IDS(14),
	
	UPDATE(7), UPDATE_BY_CRITERIA(11), UPDATE_BY_PRIMARY_KEY(13),
	
	DELETE(8), DELETE_BY_CRITERIA(12),
	
	INSERT(2), INSERT_TABLE(3), INSERT_DATABASE_TABLE(4), INSERT_BATCH(10),
	
	CLEAR(9), SAVE(10);

	public final int type;

	Method(int type) {
		this.type = type;
	}

}

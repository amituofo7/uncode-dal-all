package cn.uncode.dal.router.strategy;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.dal.router.TableRouter;

public class ShardingStrategyFactory {
	
	private static Map<String, ShardingStrategy> ALL = new HashMap<>();
	
	public static ShardingStrategy loadStrategy(TableRouter tableRouter){
		
		ShardingStrategy shardingStrategy = ALL.get(tableRouter.getTableName());
		if(shardingStrategy == null && StringUtils.isNotBlank(tableRouter.getClazz())){
			try {
				shardingStrategy = (ShardingStrategy) ShardingStrategyFactory.class.getClassLoader().loadClass(tableRouter.getClazz()).newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			if(shardingStrategy != null){
				ALL.put(tableRouter.getTableName(), shardingStrategy);
			}
		}
		return shardingStrategy;
	}
	

}

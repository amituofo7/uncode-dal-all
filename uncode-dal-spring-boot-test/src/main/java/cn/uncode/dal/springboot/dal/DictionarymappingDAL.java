package cn.uncode.dal.springboot.dal;

import cn.uncode.dal.springboot.dto.Dictionarymapping;

import cn.uncode.dal.external.CommonDAL;
 /**
 * service接口类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-04-29
 */
public interface DictionarymappingDAL extends CommonDAL<Dictionarymapping> {

}